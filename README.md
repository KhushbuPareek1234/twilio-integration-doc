Integrate Flex with Salesforce
With out-of-the-box support for Salesforce Open CTI, you can now power your Salesforce environment with omnichannel communications offered by Flex. Both Salesforce Classic and Lightning Experience integrations are supported, allowing you to use Flex directly within your Salesforce instance.
In this guide, you will learn how to configure Flex within Salesforce, configure outbound calling, enable SSO (Single Sign-On), and launch your integration.
Please read the release notes for a complete list of supported features.
Let's get started!
Visit Below Link For Complete Twilio Setup Guide->

https://www.twilio.com/docs/flex/integrations/salesforce
